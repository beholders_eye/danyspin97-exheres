# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Copyright 2019 Maxime SORIN <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='jwilm' tag=v${PV} force_git_clone=true ] cargo zsh-completion bash-completion

export_exlib_phases src_prepare src_compile src_install

SUMMARY="A cross-platform, GPU-accelerated terminal emulator"
DESCRIPTION="Alacritty is focused on simplicity and performance. The
performance goal means it should be faster than any other terminal emulator
available. The simplicity goal means that it doesn't have features such as
tabs or splits (which can be better provided by a window manager or terminal
multiplexer) nor niceties like a GUI config editor."

LICENCES="Apache-2.0"
SLOT="0"

MYOPTIONS="
    zsh-completion
    bash-completion
"

DEPENDENCIES="
    build+run:
        media-libs/fontconfig
        media-libs/freetype:=
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/libXxf86vm
    run:
        x11-utils/xclip
"

BUGS_TO="danyspin97@protonmail.com"

alacritty_src_compile() {
    cargo_src_compile

    edo mkdir terminfo
    edo tic -x -o terminfo extra/alacritty.info
}

ZSH_COMPLETIONS=( extra/completions/_alacritty )
BASH_COMPLETIONS=( extra/completions/alacritty.bash )

alacritty_src_prepare(){
    edo sed -e /Icon/s/Alacritty/Terminal/ -i extra/linux/alacritty.desktop
}

alacritty_src_install(){
    cargo_src_install

    insinto /usr/share/applications
    doins extra/linux/alacritty.desktop

    insinto /usr/share/terminfo/a
    doins terminfo/a/*

    emagicdocs

    optionq zsh-completion && zsh-completion_src_install
    optionq bash-completion && bash-completion_src_install
}
