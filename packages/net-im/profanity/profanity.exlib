# Copyright 2014 Jakob Nixdorf <flocke@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user="boothj5" ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.13 1.12 1.11 1.10 ] ]
require python [ blacklist="3.1" with_opt=true multibuild=false ]

SUMMARY="A console based XMPP client"
HOMEPAGE="http://www.profanity.im"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    libnotify
    otr [[ description = [ Support message encryption using OTR (XEP-0364) ] ]]
    pgp [[ description = [ Support message encryption using PGP (XEP-0027) ] ]]
    tray [[ description = [ Enable GTK tray icons ] ]]
    xscreensaver [[ description = [ Use libXScrnSaver to determine the idle time ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/expat
        dev-libs/glib:2[>=2.40]
        dev-libs/libmesode[>=0.9.2]
        net-misc/curl
        sys-libs/ncurses
        libnotify? ( x11-libs/libnotify )
        otr? ( net-libs/libotr[>=4.0] )
        pgp? ( app-crypt/gpgme )
        tray? ( x11-libs/gtk+:2[>=2.24.10] )
        xscreensaver? (
            x11-libs/libX11
            x11-libs/libXScrnSaver
        )
    test:
        dev-util/cmocka
"

BUGS_TO="flocke@shadowice.org"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-plugins' '--enable-c-plugins' )
DEFAULT_SRC_CONFIGURE_OPTIONS=( "python PYTHON_VERSION=${PYTHON_ABIS}" )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'libnotify notifications'
    'otr'
    'pgp'
    'python python-plugins'
    'tray icons'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'xscreensaver' )

